from stfu_tg import Doc


def test_doc():
    assert str(Doc("foo", "bar")) == "foo\nbar"
    assert str(Doc("foo", "bar").add("baz")) == "foo\nbar\nbaz"
    assert str(Doc("foo", "bar") + "baz") == "foo\nbar\nbaz"
    assert str(Doc("foo", "bar") + Doc("foo", "bar")) == "foo\nbar\nfoo\nbar"
    doc = Doc("foo", "bar")
    doc += "baz"
    assert str(doc) == "foo\nbar\nbaz"

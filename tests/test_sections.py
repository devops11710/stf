from stfu_tg import Section, VList


def test_section():
    assert str(Section('foo', 'bar', title='Title')) == '<b><u>Title</u></b>:\n  foo\n  bar'
    assert str(Section('foo', 'bar', title='Title', title_underline=False)) == '<b>Title</b>:\n  foo\n  bar'
    assert str(Section('foo', 'bar', title='Title', title_bold=False)) == '<u>Title</u>:\n  foo\n  bar'
    assert str(Section('foo', 'bar', title='Title', title_underline=False, title_bold=False)) == 'Title:\n  foo\n  bar'
    assert str(Section('foo', 'bar', title='Title', indent=2)) == '<b><u>Title</u></b>:\n    foo\n    bar'
    assert str(Section('foo', 'bar', title='Title', indent_text='-')) == '<b><u>Title</u></b>:\n-foo\n-bar'
    assert str(Section('foo', 'bar', title='Title', postfix='=')) == '<b><u>Title</u></b>=\n  foo\n  bar'


def test_no_title():
    assert str(Section('foo', 'bar')) == '\n  foo\n  bar'
    assert str(Section('foo', 'bar', indent=2)) == '\n    foo\n    bar'


def test_no_title_no_indent():
    assert str(Section('foo', 'bar', indent=0)) == '\nfoo\nbar'


def test_folded_indent():
    assert str(
        Section(
            Section('123', title='Folded', title_bold=False, title_underline=False),
            '321',
            title='Origin',
            title_bold=False,
            title_underline=False
        )) == 'Origin:\n  Folded:\n    123\n  321'


def test_superfolded():
    sec = Section(
        Section(
            Section(
                Section(
                    "foobar",
                    VList("foo", "bar"),
                    title="Fourth", title_bold=False, title_underline=False),
                title="Third", title_bold=False, title_underline=False
            ),
            title="Second", title_bold=False, title_underline=False
        ),
        title="First", title_bold=False, title_underline=False
    )

    assert str(sec) == 'First:\n  Second:\n    Third:\n      Fourth:\n        foobar\n        - foo\n        - bar'


def test_vlist():
    assert str(VList("foo", "bar")) == ' - foo\n - bar'
    assert str(VList("foo", "bar", indent=2)) == '  - foo\n  - bar'
    assert str(VList("foo", "bar", prefix='-')) == ' -foo\n -bar'


def test_vlist_in_section():
    assert str(Section(VList("foo", "bar"))) == '\n  - foo\n  - bar'
    assert str(Section(VList("foo", "bar"), indent=2)) == '\n    - foo\n    - bar'
    assert str(Section(VList("foo", "bar"), title='Section')) == '<b><u>Section</u></b>:\n  - foo\n  - bar'

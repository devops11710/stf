from dataclasses import dataclass
from typing import Any


class Core:
    pass


@dataclass
class Doc(Core, list):
    # TODO: missing docstring
    def __iter__(self):
        return filter(None, super().__iter__())

    def __str__(self) -> str:
        return '\n'.join(map(str, self))
